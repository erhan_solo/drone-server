# drone-server

central server for monitoring drones

## Usage

> npm run start

## Description

This project represents the central server that monitors the updates sent by drones.

Endpoints

- `patch /drone/:droneUUID` - updates drone state with given uuid;
  - Note that drones will send only diffs from their previous state to reduce the redundant data;
  - Thus the server must always be on, otherwise updates buffering should be implemented in drones' codebase, and once the server comes back the buffered updates are zipped and sent to the server;
  - The communication protocol is json over http which is quite bad. If the drones use modem internet to send data, then a better protocol must be used - eg. [Protocol Buffers](https://developers.google.com/protocol-buffers/);
- `get /drone/statuses` - list available drone statuses
  - Stopped
  - Ready
  - Flying
  - Recharging
- `get /drone` - list all drones alongside their state
- `get /drone/stream` - opens drone state stream
  - this endpoint opens a events-stream which will send drone updates;
  - the main endpoint used by the dashboard which updates in real-time;

This project was started from scratch and the only major dependency used was [express](https://developers.google.com/protocol-buffers/). The current structure suffices to the initial requirements. However, major changes in requirements and if lots of features are to be introduced, then a better start would be to go with a fully fledged framework like [loopback](https://loopback.io/).

## Other considerations

Right now the backend server only has linting / prettier support for code linting / formatting. Thus, the following list of items must be completed to ensure the stability fo the server.

- [x] Linting
- [x] Auto Code Formatting
- [ ] Unit Tests
- [ ] End To End tests
  - The main scenarios here should be:
    - Sending drone updates produces the correct drones state
    - Sending drone updates correctly streams drone-updates back via drone-stream endpoint

### Licence

MIT.
