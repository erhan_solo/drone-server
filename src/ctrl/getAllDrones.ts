import { Response } from 'express';
import { default as DroneStore } from '../repo/drone_store';

export function getAllDrones(__, res: Response) {
  res.status(200).send(DroneStore.getDrones());
}
