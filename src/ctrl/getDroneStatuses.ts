import { Response } from 'express';
import { DroneStatus } from '../repo/drone';

export function getDroneStatuses(__, res: Response) {
  res.status(200).send(DroneStatus);
}
