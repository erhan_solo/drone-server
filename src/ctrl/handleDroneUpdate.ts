import { Request, Response } from 'express';
import { DroneUpdates } from '../repo/drone';
import { default as DroneStore } from '../repo/drone_store';

export function handleDroneUpdate(req: Request, res: Response) {
  const uuid = req.params.droneUUID;
  const updates: DroneUpdates = { ...req.body, uuid };

  DroneStore.handleDroneUpdates(updates);
  res.status(200).end();
}
