import { Request, Response } from 'express';
import { Drone, DroneUpdates } from '../repo/drone';
import {
  default as DroneStore,
  DroneUpdateOp,
  OperationType
} from '../repo/drone_store';

// tslint:disable:no-console
// define loggers
const log = console.log.bind(console);
const eLog = console.error.bind(console);
// tslint:enable:no-console

const SSEHeaders = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'text/event-stream',
  'Cache-Control': 'no-cache',
  Connection: 'keep-alive'
};

export function openDroneStream(req: Request, res: Response) {
  let id = 1;
  let subId;

  const sendUpdates = (d: Drone, du: DroneUpdateOp) => {
    let dataToSend: DroneUpdates;

    switch (du.action) {
      case OperationType.Add:
        dataToSend = d;
        break;
      case OperationType.Update:
        dataToSend = du.data;
        break;
    }

    res.write(`id:${id++}\n`);
    res.write(`event:${du.action}\n`);
    res.write(`data:${JSON.stringify(dataToSend)}\n\n`);
  };

  const hangup = () => {
    log('Closing data stream');
    res.end();
    DroneStore.unSubChange(subId);
    log('Data stream closed');
  };

  // set headers to Server-Sent Events
  res.writeHead(200, SSEHeaders);

  // in cases when the client hangs, we must have an error
  // handler
  res.on('error', () => {
    eLog('catched data stream error; closing connection with client');
    hangup();
  });

  // send all the existing drones from repo;
  DroneStore.getDrones().forEach(d =>
    sendUpdates(d, { action: OperationType.Add, data: d })
  );
  // subscribe to drone changes
  subId = DroneStore.subChange(sendUpdates);

  req.on('close', hangup);
}
