interface DroneProperties {
  status: DroneStatus;
  currentLocation: GeoLocation;
  destination: GeoLocation;
  currentSpeed: number;
  lastFlyTime: Date;
}

export enum DroneStatus {
  Flying = 'Flying',
  Ready = 'Ready',
  Recharging = 'Recharging',
  Stopped = 'Stopped'
}

export interface GeoLocation {
  lat: number;
  lng: number;
}

export interface Drone extends DroneProperties {
  uuid: string;
}

export interface DroneUpdates extends Partial<DroneProperties> {
  uuid: string;
}
