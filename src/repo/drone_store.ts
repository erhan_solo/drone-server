import { Drone, DroneStatus, DroneUpdates } from './drone';

export type DroneUpdateSub = (d: Drone, u: DroneUpdateOp) => void;

function tryCatch(f: DroneUpdateSub): DroneUpdateSub {
  return (d: Drone, u: DroneUpdateOp) => {
    try {
      f(d, u);
    } catch {}
  };
}

class DroneStore {
  private drones: Drone[] = [];
  private changeSubscribers: DroneUpdateSub[] = [];

  public handleDroneUpdates(du: DroneUpdates): void {
    const droneIdx = this.drones.findIndex(d => d.uuid === du.uuid);
    if (droneIdx === -1) {
      this.add(du);
      return;
    } else {
      this.update(droneIdx, du);
    }
  }

  public subChange(fn: DroneUpdateSub): number {
    return this.changeSubscribers.push(tryCatch(fn));
  }

  public unSubChange(subID: number): void {
    this.changeSubscribers.splice(subID - 1, 1);
  }

  public getDrones(): Drone[] {
    return this.drones;
  }

  private emitChange(d: Drone, du: DroneUpdateOp): void {
    this.changeSubscribers.forEach(f => f(d, du));
  }

  private add(du: DroneUpdates): void {
    const c: Drone = {
      status: DroneStatus.Ready,
      uuid: '',
      currentLocation: null,
      destination: null,
      currentSpeed: 0,
      lastFlyTime: new Date(),
      ...du
    };
    this.drones.push(c);
    this.emitChange(c, { action: OperationType.Add, data: du });
  }

  private update(idx: number, updates: DroneUpdates): void {
    const d = this.drones[idx];
    if (!updates.currentLocation) {
      delete updates.currentLocation;
    }

    const updated = { ...d, ...updates };
    if (updated.status === DroneStatus.Flying) {
      updated.lastFlyTime = new Date();
      updates.lastFlyTime = new Date();
    }

    this.drones[idx] = updated;
    this.emitChange(updated, { action: OperationType.Update, data: updates });
  }
}

export enum OperationType {
  Add = 'Add',
  Remove = 'Remove',
  Update = 'Update'
}

export interface DroneUpdateOp {
  action: OperationType;
  data: DroneUpdates;
}

export default new DroneStore();
