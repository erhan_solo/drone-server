import { json } from 'body-parser';
import { Router } from 'express';
import { getAllDrones } from '../ctrl/getAllDrones';
import { getDroneStatuses } from '../ctrl/getDroneStatuses';
import { handleDroneUpdate } from '../ctrl/handleDroneUpdate';
import { openDroneStream } from '../ctrl/openDroneStream';

export function installMiddleware(s: Router) {
  s.use(json());
}

export function installRoutes(s: Router) {
  s.get('/drone/statuses', getDroneStatuses);
  s.patch('/drone/:droneUUID', handleDroneUpdate);
  s.get('/drone/stream', openDroneStream);
  s.get('/drone', getAllDrones);
}
