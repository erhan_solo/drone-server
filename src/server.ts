import express from 'express';
import { installMiddleware, installRoutes } from './route';

const server = express();

// register middleware
installMiddleware(server);

// register routes
installRoutes(server);

export default server;
